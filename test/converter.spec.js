const expect = require('chai').expect;
const converter = require('../src/converter');

describe("color code converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255, 0, 0); // ff0000
            const greenHex = converter.rgbToHex(0, 255, 0); // 00ff00
            const blueHex = converter.rgbToHex(0, 0, 255); // 0000ff

            expect(redHex).to.equal("ff0000")
            expect(greenHex).to.equal("00ff00")
            expect(blueHex).to.equal("0000ff")
        })
    })
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const r = converter.hexToRGB('ff0000');
            const g = converter.hexToRGB('00ff00');
            const b = converter.hexToRGB('0000ff');

            //const arr = [];
            //expect(arr).to.equal([])
    
            expect(r).to.deep.equal([255, 0, 0])
            expect(g).to.deep.equal([0, 255, 0])
            expect(b).to.deep.equal([0, 0, 255])
        })
    })
})